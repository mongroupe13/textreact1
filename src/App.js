// import logo from './logo.svg';
// import './App.css';
import About from './Components/about/About';
import Home from './Components/home/Home';
import Services from './Components/services/Services';


function App() {
  return (
    <div className="App">
      <Home/>
      <About/>
      <Services/>
    </div>
  );
}

export default App;
