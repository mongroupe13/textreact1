import React from 'react';
import mabout from "../assets/images/about.jpg"
import mclient1 from "../assets/images/client-1.png"
import mclient2 from "../assets/images/client-2.png"
import mclient3 from "../assets/images/client-3.png"
import mclient4 from "../assets/images/client-4.png"
import mclient5 from "../assets/images/client-5.png"
import mclient6 from "../assets/images/client-6.png"
import mclient7 from "../assets/images/client-7.png"
import mclient8 from "../assets/images/client-8.png";
import mordi from "../assets/images/ordi.jpg";

import './About.css';

function About(props) {
    return (
        <div>
            <div className="container" id='About'>
  <div className="row aboutRow">
    <div className="col-md-6">
   <h3>Voluptatem dignissimos provident quasi corporis voluptates sit assumenda.</h3>
   <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>

<p>Ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>

<p>Duis aute irure dolor in reprehenderit in voluptate velit.</p>

<p>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate trideta storacalaperda mastiro dolore eu fugiat nulla pariatur.
</p>

<p>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident</p>


    </div>
    <div className="col-md-6">
     <div className='imageabout'>
        <img src={mabout} alt=''/>
     </div>
    </div>
  
  </div>
  <div className="row deuxiemeRow">
  <div id="carouselExampleDark" className="carousel  carousel-fade slide" data-bs-ride="carousel">
  <div className="carousel-indicators">
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" className="active" aria-current="true" aria-label="Slide 1"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 4"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 5"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 6"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 7"></button>
    <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 8"></button>
  </div>
  <div className="carousel-inner">
    <div className="carousel-item active" >
      <img src={mclient1} className="d-block " alt="..."/>
      
    </div>
    <div className="carousel-item" >
    <img src={mclient2} className="d-block " alt="..."/>
    </div>
    <div className="carousel-item">
    <img src={mclient3} className="d-block " alt="..."/>
    </div>
    <div className="carousel-item">
    <img src={mclient4} className="d-block " alt="..."/>
    </div>
    <div className="carousel-item">
    <img src={mclient5} className="d-block " alt="..."/>
    </div>
    <div className="carousel-item">
    <img src={mclient6} className="d-block " alt="..."/>
    </div>
    <div className="carousel-item">
    <img src={mclient7} className="d-block " alt="..."/>
    </div>
    <div className="carousel-item">
    <img src={mclient8} className="d-block " alt="..."/>
    </div>
  </div>
  
</div>
  </div>
  <div className="row troixiemeRow">
    <div className="col-md-6">
        <div className='image2'>
            <img src={mordi} alt=""  style={{width: 400}}/>
        </div>
    </div>
    <div className="col-md-6">
        <div className='divPrincile'>
            <div className='laborde'>
                <div className='icons'></div>
                <div className='text'>
                    <h3>Est labore ad</h3>
                    <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                </div>
            </div>
            <div className='harum'>
            <div className='icons'></div>
                <div className='text'>
                    <h3>Est labore ad</h3>
                    <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                </div>
            </div>
            <div className='out'>
            <div className='icons'></div>
                <div className='text'>
                    <h3>Est labore ad</h3>
                    <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                </div>
            </div>
            <div className='veritalist'>
            <div className='icons'></div>
                <div className='text'>
                    <h3>Est labore ad</h3>
                    <p>Consequuntur sunt aut quasi enim aliquam quae harum pariatur laboris nisi ut aliquip</p>
                </div>
            </div>
        </div>
    </div>
  </div>
</div>
        </div>
    );
}

export default About;